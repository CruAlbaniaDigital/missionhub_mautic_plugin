@echo off
if not exist vendor\bin\phing.bat (
    composer install
    vendor\bin\phing.bat %*
)

vendor\bin\phing.bat %*