<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

require('hello.php');

/**
 * @covers Email
 */
final class HelloTest extends TestCase
{
    public function testSaysHello(): void
    {
        $instance = new Hello();

        //act
        $response = $instance->sayHello();

        //assert
        $this->assertEquals($response, "hello");
    }
}

?>