
## Missionhub Mautic Plugin
A plugin for [Mautic](https://www.mautic.org/) that provides integration with [MissionHub](https://www.missionhub.com/).

License: GNU GPLv3

Mautic plugin docs: https://developer.mautic.org/#development-environment

MissionHub API docs: http://api.missionhub.com/docs 
